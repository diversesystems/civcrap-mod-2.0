package mirmir.client.parttwo;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.client.event.MouseEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

import java.io.*;
import java.util.HashSet;

@Mod(modid = "dabsonmir", name = "Dabs on Mir Fun fact, I haven't played in over 8 months https://i.imgur.com/6Qr94Eo.png", version = "send me thighs as payment :/ made " +
        "you look . ")
public class DabsOnMir {

    private File configFolder;
    private static File orbiterFile;
    private static File chaserFile;

    public static HashSet<String> orbiters, chasers;

    private KeyBinding enable = new KeyBinding("Turns it on and off :/ made you look . ", 0, "Dabs On Mir :/ made you look . ");

    @Mod.EventHandler
    public void onPreInit(FMLPreInitializationEvent e) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append(Minecraft.getMinecraft().mcDataDir).append("//config//MirMir");
        configFolder = new File(sb.toString());

        if (!configFolder.exists())
            configFolder.mkdirs();

        sb = new StringBuilder();
        sb.append(configFolder).append("//MirOrbiters.txt");
        orbiterFile = new File(sb.toString());

        sb = new StringBuilder();
        sb.append(configFolder).append("//MirChasers.txt");
        chaserFile = new File(sb.toString());

        if (!orbiterFile.exists())
            orbiterFile.createNewFile();

        if (!chaserFile.exists())
            chaserFile.createNewFile();

        orbiters = new HashSet<>();
        chasers = new HashSet<>();

        loadOrbiters(orbiterFile);
        loadChasers(chaserFile);

    }

    @Mod.EventHandler
    public void onInit(FMLInitializationEvent e) {
        MinecraftForge.EVENT_BUS.register(this);
        FMLCommonHandler.instance().bus().register(this);

        ClientRegistry.registerKeyBinding(enable);

        ClientCommandHandler.instance.registerCommand(new CommandAdd());
        ClientCommandHandler.instance.registerCommand(new CommandRemove());
        MinecraftForge.EVENT_BUS.register(new EventHandler(this));
    }

    @SubscribeEvent
    public void onWorldJoin(EntityJoinWorldEvent e) {
        if (e.getEntity() == Minecraft.getMinecraft().player)
            createTeams();
    }

    private void createTeams() {
        if (!Minecraft.getMinecraft().world.getScoreboard().getTeamNames().contains("Orbiters") && !Minecraft.getMinecraft().world.getScoreboard().getTeamNames().contains("Incels") && !Minecraft.getMinecraft().world.getScoreboard().getTeamNames().contains("Chasers")) {
            Minecraft.getMinecraft().world.getScoreboard().createTeam("Orbiters").setPrefix(TextFormatting.GREEN.toString());

            Minecraft.getMinecraft().world.getScoreboard().createTeam("Chasers").setPrefix(TextFormatting.RED.toString());

            Minecraft.getMinecraft().world.getScoreboard().createTeam("Incels").setPrefix(TextFormatting.YELLOW.toString());
        }
    }

    @SubscribeEvent
    public void onKeyInput(final InputEvent.KeyInputEvent event) throws Exception {
        if (this.enable.isPressed()) {
            EventHandler.enable = !EventHandler.enable;
        }
    }

    @SubscribeEvent
    public void onMouse(MouseEvent e) throws Exception {
        if (!e.isButtonstate())
            return;

        RayTraceResult pos;
        String name;

        if (e.getButton() == 2) {
            pos = Minecraft.getMinecraft().objectMouseOver;

            if (pos == null || pos.entityHit == null || !(pos.entityHit instanceof EntityPlayer) || !pos.typeOfHit.equals(RayTraceResult.Type.ENTITY))
                return;

            name = pos.entityHit.getName();

            if (!orbiters.contains(name) && !chasers.contains(name)) {
                orbiters.add(name);
                Minecraft.getMinecraft().player.sendMessage(new TextComponentString("Added " + name + " to orbiters"));
            } else if (orbiters.contains(name)) {
                orbiters.remove(name);
                Minecraft.getMinecraft().player.sendMessage(new TextComponentString("Removed " + name + " from orbiters"));
            }

            whipOrbiters();
        }
    }

    private void loadChasers(File enemiesFile) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(enemiesFile));
        String read = br.readLine();

        while (read != null) {
            if (!read.trim().isEmpty()) {
                if (!chasers.contains(read) && !orbiters.contains(read)) {
                    chasers.add(read);
                }
            }
            read = br.readLine();
        }

        br.close();
    }

    private void loadOrbiters(File of) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(of));
        String read = br.readLine();

        while (read != null) {
            if (!read.trim().isEmpty()) {
                if (!chasers.contains(read) && !orbiters.contains(read)) {
                    orbiters.add(read);
                }
            }
            read = br.readLine();
        }

        br.close();
    }

    public static void whipOrbiters() throws Exception {
        BufferedWriter bw = new BufferedWriter(new FileWriter(orbiterFile));

        String[] temp = orbiters.toArray(new String[0]);
        StringBuilder sb;

        for (String s : temp) {
            if (s != null && !s.isEmpty()) {
                sb = new StringBuilder();
                sb.append(s).append("\n");
                bw.write(sb.toString());
            }
        }
        bw.close();
    }

    public static void chaseDegenerates() throws Exception {
        BufferedWriter bw = new BufferedWriter(new FileWriter(chaserFile));

        String[] temp = chasers.toArray(new String[0]);
        StringBuilder sb;

        for (String s : temp) {
            if (s != null && !s.isEmpty()) {
                sb = new StringBuilder();
                sb.append(s).append("\n");
                bw.write(sb.toString());
            }
        }
        bw.close();
    }
}
