package mirmir.client.parttwo;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventHandler {
    public static boolean enable = true;

    public EventHandler(DabsOnMir dabsOnMir) {
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onPlayerRenderPost(RenderPlayerEvent.Post e) {
        if (e != null && Minecraft.getMinecraft().world != null && enable) {
            if (isValid(e.getEntity())) {
                String obsession = checkObsession(e.getEntity().getName());

                outlineObsession(e.getEntity(), obsession);
            }
        } else if (!enable) {
            if (e.getEntity().isGlowing())
                e.getEntity().setGlowing(false);
        }
    }

    private void outlineObsession(Entity entity, String obsession) {

        if (Minecraft.getMinecraft().world.getScoreboard().getTeamNames().contains("Orbiters") && obsession.equals("orbiting")) {
            Minecraft.getMinecraft().world.getScoreboard().addPlayerToTeam(entity.getName(), "Orbiters");
        } else if (Minecraft.getMinecraft().world.getScoreboard().getTeamNames().contains("Chasers") && obsession.equals("chasing")) {
            Minecraft.getMinecraft().world.getScoreboard().addPlayerToTeam(entity.getName(), "Chasers");
        } else if (Minecraft.getMinecraft().world.getScoreboard().getTeamNames().contains("Incels") && obsession.equals("incel")) {
            Minecraft.getMinecraft().world.getScoreboard().addPlayerToTeam(entity.getName(), "Incels");
        }

        if (!entity.isGlowing())
            entity.setGlowing(true);
    }

    private String checkObsession(String name) {
        if (DabsOnMir.orbiters.contains(name))
            return "orbiting";
        else if (DabsOnMir.chasers.contains(name))
            return "chasing";
        else
            return "incel";
    }

    private boolean isValid(Entity entity) {
        return entity != null && entity != Minecraft.getMinecraft().player && !entity.isDead && entity.isEntityAlive() && entity instanceof EntityPlayer && entity instanceof EntityOtherPlayerMP;
    }
}
